(function($) {
  Drupal.behaviors.views_simplechart = {
    attach: function(context, settings) {
      google.charts.load('current', {
        'packages': ['corechart','timeline','orgchart']
      });

      function views_simplechart_render_chart(settings, id) {
        google.charts.setOnLoadCallback(function() {
          var data = new google.visualization.arrayToDataTable(JSON.parse(settings.data));
          var options = {
            is3D: true,
            legend: settings.chart_legend_position,
            title: settings.chart_title,
            width: settings.chart_width,
            height: settings.chart_height,
            allowHtml: true
          };
          if ($.inArray(settings.chart_type, ['BarChart', 'ColumnChart']) && settings.chart_type_stacked == 'yes') {
            options['isStacked'] = true;
          }
          var container = document.getElementById(id);
          if (container) {
            var chart = new google.visualization[settings.chart_type](container);
            chart.draw(data, options);
          }
        });
      }
      for (var id in Drupal.settings.views_simplechart) {
        views_simplechart_render_chart(Drupal.settings.views_simplechart[id], id);
      }

    }
  }
})(jQuery);
